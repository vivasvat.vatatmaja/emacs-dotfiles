;;; _____  __   _  _____  _______      ______   _      ;;;
;;;|__ __||\ \ | ||__ __||_______|    | _____| | |     ;;;
;;;  | |  | \ \| |  | |     | |       | |____  | |     ;;;
;;;  | |  | |\ \ |  | |     | |       |  ____| | |     ;;;
;;; _| |_ | | \ \| _| |_    | |    _  | |____  | |___  ;;;
;;;|_____||_|  \_||_____|   |_|   |_| |______| |_____| ;;;


;;; Code:
; Package Initialization: --------------------------------
(require 'package)

(setq package-selected-packages '(evil
;;				    exwm
				    undo-tree
				    ivy
				    counsel
				    swiper
				    auctex
                                    omnisharp
;;				    js2-mode
;;				    lua-mode
				    clojure-mode
				    cider-mode
				    magit
				    company
				    irony
				    company-irony
				    company-rtags
;;				    rtags
                                    neotree
				    powerline
                                    powerline-evil
				    flycheck
				    doom-themes
				    pdf-tools
				    yasnippet))

(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/"))

(package-initialize)

(unless package-archive-contents
 package-refresh-contents)
(package-install-selected-packages)

;;;; File Settings: ------------------------------------------
;; Set init file.
(setq initial-buffer-choice "~/Documents/Vivasvat/Org-Mode/Org-Mode.org" )

;; load init packages
(add-to-list 'load-path "~/.emacs.d/init/")

;; load site-lisp
(add-to-list 'load-path "~/.emacs.d/site-lisp/")

;; theme directory
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

;; key chords library -- TRY TO REMOVE THIS IN THE COMING WEEKS!!!!!!
(setq key-chord-library "~/.emacs.d/site-lisp/key-chord/key-chord.el")
(load key-chord-library)

;; seperate custom file
(setq custom-file "~/.emacs.d/.custom.el")
(load custom-file)

;; back up files
(setq backup-directory-alist '(("." . "~/.emacs.d/.backups")))
(setq backup-by-copying t)

(setq delete-old-versions t
      kept-new-versions 20
      kept-old-versions 10
      version-control t)

;; Adding things to Path
;; (if (eq system-type 'darwin)
;; (setenv "PATH" (concat (getenv "PATH") ":/Library/TeX/texbin"))
;; (setq exec-path (append exec-path '("/Library/TeX/texbin")))
;; )

;;; Package Settings: --------------------------------------
;;(require 'init-exwm)
(require 'init-orgmode)
(require 'init-auctex)
(require 'init-flycheck)
(require 'init-neotree)
(require 'init-js2)
(require 'init-company)
(require 'init-irony)
;; (require 'rtags)
(require 'init-magit)
(require 'init-mac)
(require 'init-gnus)
(require 'init-evil)
(require 'init-ivy)
(require 'init-misc)
(require 'init-myfuncs)
(require 'init-pdftools)
(require 'init-yasnippet)


(provide 'init)
;;; init.el ends here
