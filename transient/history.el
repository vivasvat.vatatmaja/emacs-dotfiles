((magit-commit nil)
 (magit-diff
  ("--no-ext-diff" "--stat"))
 (magit-dispatch nil)
 (magit-log
  ("-n256" "--graph" "--color" "--decorate")
  ("-n256" "--graph" "--decorate"))
 (magit-pull nil)
 (magit-push nil)
 (magit-remote
  ("-f"))
 (magit-stash nil))
