;;; init-ivy --- turning on ivy and counsel
;;; Commentary:
;;; Code:
;; (require 'counsel)
(require 'ivy)

(ivy-mode)
(global-set-key (kbd "C-s") 'swiper-isearch)

(provide 'init-ivy)
;;; init-ivy ends here
