;;;;;; Enable zap-up-to-char ;;;;;;
(require 'init-orgmode)
(autoload 'zap-up-to-char "misc"
  "Kill up to, but not including the ARGth occurence of CHAR.
\(fn arg char)"
  'interactive)

;;;; Bind zap-up-to-char ;;;;
(global-set-key "\M-Z" 'zap-up-to-char)

;;;;;; Text Object Zap ;;;;;;
(defun my/ZapInside (arg char)
  "Zap Around cursor to character point"
  (interactive "p\ncZap within char: ")
  (zap-up-to-char (* arg 1) char)
  (zap-up-to-char (* arg -1) char))

;; Bind my/ZapInside ;;
(global-set-key (kbd "C-c z") 'my/ZapInside)

;; function to jump to Org-Master
(defun my/ToOrgMaster ()
   "Function to go to master Org Document..."
   (interactive)
   (find-file master-org-path))

; binding my/ToOrgMaster
(global-set-key (kbd "C-c m") 'my/ToOrgMaster)

;; (defvar *my/theme-dark 'challenger-deep)
;; (defvar *my/light-theme 'doom-solarized-light)
;; (defvar *my/current-theme 'doom-solarized-light)
;; 
;; (defun my/toggle-theme ()
;;   (interactive)
;;   (if (eq my/current-theme *my/theme-dark) 

(provide 'init-myfuncs)
