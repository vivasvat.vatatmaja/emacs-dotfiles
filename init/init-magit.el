;;; init-magit --- configuration for magit git interface
;;; Commentary:

;;; Code:
(require 'magit)
(global-set-key (kbd "C-x g") 'magit-status)

(provide 'init-magit)
;;; init-magit ends here
