;;; Package --- My flycheck settings

;;; Commentary:
;;; Just hooks

;;; Code:
(add-hook 'c-mode-hook 'flycheck-mode)
(add-hook 'c++-mode-hook 'flycheck-mode)
(add-hook 'js-mode-hook 'flycheck-mode)
(add-hook 'emacs-lisp-mode-hook 'flycheck-mode)
(add-hook 'csharp-mode-hook 'flycheck-mode)

(provide 'init-flycheck)

;;;  init-flycheck.el ends here
