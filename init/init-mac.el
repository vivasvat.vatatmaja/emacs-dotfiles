;; To make modifiers bearable on mac

(if (eq system-type 'darwin)
    (setq mac-option-modifier 'meta)
 (setq mac-command-modifier 'hyper))

(provide 'init-mac)
