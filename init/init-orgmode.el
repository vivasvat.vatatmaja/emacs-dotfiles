;;; init-orgmade ----- Orgmode configuration -----
;;; Commentary:
; You will need to make sure that you load whatever directory this file is in
; The easiest way is to do (add-to-list 'load-path "/path/to/directory/containing/file/)

;;; Author: Vivasvat Vatatmaja

;;; imports
;; Org-Bullets ;;
;;; Code:
(require 'ob-shell)
(require 'org-bullets)

(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
(add-hook 'org-mode-hook 'visual-line-mode)
(add-hook 'org-mode-hook 'variable-pitch-mode)

;; Adding C to babel-org execution ;;
(org-babel-do-load-languages
 'org-babel-load-languages '((python . t) (emacs-lisp . t) (shell . t) (latex . t) (C . t)))

; (setq org-latex-create-formula-image-program 'dvisvgm)

(if (eq system-type 'darwin)
    (defconst master-org-path "~/Documents/Vivasvat/Vivasvat/Org-Mode/Org-Mode.org")
  )
(if (eq system-type 'w32)
    (defconst master-org-path "C:\\Users\\vivas\\Documents\\Org-Mode\\Org-Mode.org")
  )
(if (eq system-type 'gnu/linux)
;; (setq variable-face et-bembo)
;;  (add-hook 'org-mode-hook 'variable-pitch-mode)
;;(defconst org-archive-location "~/Documents/Vivasvat/Org-Mode/Archive/MasterArchive.org")

;; (set-face-attribute 'org-document-title t :foreground "light coral" :height 1.5)
;; (set-face-attribute 'org-level-1 t :foreground "#ffffff" :weight 'bold :height 1.3)
;; (set-face-attribute 'org-level-3 t :foreground "HotPink1")
;; (set-face-attribute 'org-level-4 t :foreground "turquoise")
;; (set-face-attribute 'org-link t :foreground "deep sky blue" :underline t)
;; '(variable-pitch ((t (:family "ETBembo" :height 180 :weight thin))))
;; '(fixed-pitch ((t ( :family "Fira Code Retina" :height 160))))
    (defconst master-org-path "~/Documents/Vivasvat/Org-Mode/Org-Mode.org")
  )

;; function to jump to Org-Master
(defun my/ToOrgMaster ()
   "Function to go to master Org Document..."
   (interactive)
   (find-file master-org-path))

;; Set background depending on mode
;; (defun my/set-theme-on-mode ()
;;   "Set background color for different modes."
;;   (interactive)
;;   (let ((fileNameSuffix (file-name-extension (buffer-file-name) ) ))
;;     (cond
;;      ((string= fileNameSuffix "org" ) (set-background-color "honeydew"))
;;      ((string= fileNameSuffix "txt" ) (set-background-color "cornsilk"))
;;      (t (message "%s" "no match found"))
;;      )
;;     ))

;; (add-hook 'find-file-hook 'my/set-theme-on-mode)

; binding my/ToOrgMaster
(global-set-key (kbd "C-c m") 'my/ToOrgMaster)

; binding for org-agenda
(global-set-key (kbd "C-c a") 'org-agenda)

(provide 'init-orgmode)
;;; init-orgmode ends here
