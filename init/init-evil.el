(require 'evil)

;;; Code:
;; Binding for invoking evil ;;
(global-set-key (kbd "C-c e") 'evil-mode)

(evil-mode)
(global-undo-tree-mode -1)

(evil-set-leader 'normal (kbd "SPC"))
(evil-define-key 'normal 'global (kbd "<leader>xs") 'save-buffer)
(evil-define-key 'normal 'global (kbd "<leader>cm") 'my/ToOrgMaster)
(evil-define-key 'normal 'global (kbd "<leader>xf") 'find-file)
				  
(evil-define-key 'normal 'emacs-lisp-mode-hook (kbd "<leader>xe") 'eval-last-sexp)


(setcdr evil-insert-state-map nil)
(define-key evil-insert-state-map [escape] 'evil-normal-state)
(setq key-chord-two-keys-delay 0.5)
(key-chord-define evil-insert-state-map "jj" 'evil-normal-state)
(key-chord-mode 1)

(provide 'init-evil)
;;; init-evil.el ends here
