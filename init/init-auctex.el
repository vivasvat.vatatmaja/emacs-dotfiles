;;; init-auctex ------ auctex configuration
;;; Commentary:

;;; Code:

;; (load "auctex.el" nil t t)
;; (load "preview-latex.el" nil t t)

(setq TeX-auto-save t)
(setq TeX-parse-self t)

(setq-default TeX-master nil)

(set-default 'preview-scale-function 1.8)
;; add prettify-ed symbols
(add-hook 'TeX-mode-hook 'prettify-symbols-mode)


(provide 'init-auctex)
;;; init-auctex ends here
