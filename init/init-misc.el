;;; misc --- miscellaneous settings and variables
;;; Commentary:
; You will need to make sure that you load whatever directory this file is in
; The easiest way is to do (add-to-list 'load-path "/path/to/file/)

;;; Author: Vivasvat Vatatmaja

;;; Code:
;; aesthetics
; (if (display-graphic-p)
     (load-theme 'doom-solarized-light) ;t)
;    (load-theme 'challenger-deep) ;)

(require 'powerline)
(require 'doom-modeline)

(if (display-graphic-p)
    (doom-modeline-mode 1)
  nil)

(setq doom-modeline-modal-icon t)

;;;;; other hooks that don't merit their own file ----
(add-hook 'csharp-mode-hook 'omnisharp-mode)

;;;;; defuns and bindings ----
;; increment number at point defun
(defun my/increment-number-at-point ()
  "Increment number under cursor."
  (interactive)
  (skip-chars-backward "0-9")
  (or (looking-at "[0-9]+")
      (error "No number at point"))
  (replace-match (number-to-string (1+ (string-to-number (match-string 0))))))

;; Enable zap-up-to-char
(autoload 'zap-up-to-char "misc"
  "Kill up to, but not including the ARGth occurence of CHAR.
\(fn arg char)"
  'interactive)

;; Text Object
(defun my/ZapInside (arg char)
  "Zap Around cursor to character (ARG: CHAR) point."
  (interactive "p\ncZap within char: ")
  (zap-up-to-char (* arg 1) char)
  (zap-up-to-char (* arg -1) char))

;; Set background depending on mode
;; (defun my/set-theme-on-mode ()
;;   "Set background color for different modes."
;;   (interactive)
;;   (let ((fileNameSuffix (file-name-extension (buffer-file-name) ) ))
;;     (cond
;;      ((string= fileNameSuffix "org" ) (set-background-color "honeydew"))
;;      ((string= fileNameSuffix "txt" ) (set-background-color "cornsilk"))
;;      (t (message "%s" "no match found"))
;;      )
;;     ))

;;; Custom Bindings
(global-set-key "\M-Z" 'zap-up-to-char) ; Bind zap-up-to-char
(global-set-key (kbd "C-c z") 'my/ZapInside) ; Bind my/ZapInside
(global-set-key (kbd "C-c +") 'my/increment-number-at-point) ; Bind increment

;;;;; Variables ---
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)

(setq shr-color-visible-luminance-min 90)
(setq prettify-symbols-unprettify-at-point t)

;;;;; misc misc
(windmove-default-keybindings) ;; initializing windmove
(ivy-mode) ;; initializing ivy
(server-start) ;; initializing emacs-server

(provide 'init-misc)
;;; init-misc.el ends here
