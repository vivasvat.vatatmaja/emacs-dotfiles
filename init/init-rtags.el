(require 'rtags)
(require 'company)

(add-hook 'c-mode-hook 'rtags-start-process-unless-running)
(add-hook 'c++-mode-hook 'rtags-start-process-unless-running)
(add-hook 'objc-mode-hook 'rtags-start-process-unless-running)

(setq rtags-completions-enabled t)
(define-key c-mode-base-map (kbd "<C-tab>") (function company-complete))

(provide 'init-rtags)
