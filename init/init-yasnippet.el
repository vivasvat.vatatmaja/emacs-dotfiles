;;; init-yasnippet ------ auctex configuration
;;; Commentary:

;;; Code:

(require 'yasnippet)

(setq yas-snippet-dirs
      '("~/.emacs.d/site-lisp/snippets/"))

(add-hook 'TeX-mode-hook 'yas-reload-all)
(add-hook 'TeX-mode-hook #'yas-minor-mode)

(provide 'init-yasnippet)
;;; init-yasnippet.el ends here
