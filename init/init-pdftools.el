;;; init-pdftools --- configuration for pdf tools interface
;;; Commentary:

;;; Code:
(require 'pdf-tools)

(pdf-loader-install)

(provide 'init-pdftools)
;;; init-pdftools ends here
