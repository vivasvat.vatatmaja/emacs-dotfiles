;;;;;;;;;;;;;;; Neotree ;;;;;;;;;;;;;;;

(setq neo-theme (if (display-graphic-p) 'icons 'arrow))

;; setting all-the-icons as theme
;; to use, make sure all the icons is installed

(provide 'init-neotree)
