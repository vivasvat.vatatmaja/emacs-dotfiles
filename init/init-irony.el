(require 'irony)
(require 'company-irony)

;; adding hooks for C/C++, Objective C
(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
(add-hook 'objc-mode-hook 'irony-mode)

;; adding special hook for compiling
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(if (eq system-type 'windows-nt)
    (setq w32-pipe-read-delay 0))

(provide 'init-irony)

;; https://www.reddit.com/r/emacs/comments/7na37q/how_can_i_get_decent_c_autocompletion/ <-- details irony
