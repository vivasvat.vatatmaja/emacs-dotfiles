(defun my-gnus-faces ()
  "Buffer-local face remapping for `gnus-article-mode-hook'."
  (face-remap-add-relative 'default
                           :background "White"
                           :foreground "Black"))
(add-hook 'gnus-article-mode-hook #'my-gnus-faces)

(provide 'init-gnus)
