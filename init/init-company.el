(require 'company)
(require 'company-irony)

;;; Code:

;; adding hooks for C/C++
(add-hook 'c-mode-hook 'company-mode)
(add-hook 'c++-mode-hook 'company-mode)
(add-hook 'csharp-mode-hook #'company-mode)

;; adding hooks for elisp
(add-hook 'emacs-lisp-mode-hook 'company-mode)

;; adding company backends
(eval-after-load 'company
  '(add-to-list 'company-backends '(company-irony company-clang company-omnisharp company-rtags company-dabbrev)))

;; speeding up company
(setq company-dabbrev-downcase 0)
(setq company-idle-delay 0.3)

(provide 'init-company)
